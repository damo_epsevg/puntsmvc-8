package edu.upc.damo.punts_mvc;

import android.app.Activity;
import android.app.AlertDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;

import java.util.Random;

public class MainActivity extends Activity {

    static final String TAG = "PROVA";

    private EditText t1;
    private EditText t2;

    private CjtDePunts cjtDePunts = new CjtDePunts();       // El Model
    private VistaDePunts vista;                          // La vista

    private VistaPunt vistaPunt; // Vista d'un punt

    private Random generador = new Random();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        inicialitza();

        vista.defineixModel(cjtDePunts);
        vistaPunt = new VistaPunt(t1, t2);
    }


    /* Inicialització de l'activity */

    private void inicialitza() {
        t1 = (EditText) findViewById(R.id.text1);
        t2 = (EditText) findViewById(R.id.text2);
        vista = (VistaDePunts) findViewById(R.id.vistaDePunts);


        final GeneradorDePunt g = new GeneradorDePunt();

        // En aquest moment la view encara no existeix, i per tant no té mesures.
        // De moment creem el generador, i més endavant ja li indicarem els límits

        findViewById(R.id.botoVerd).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nouPunt(g.obtenirGeneradorPunt(null), null, R.color.colorBotoVerd);
                    }
                }
        );

        findViewById(R.id.botoVermell).setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        nouPunt(g.obtenirGeneradorPunt(null), null, R.color.colorBotoVermell);
                    }
                }
        );

        vista.setOnTouchListener(new View.OnTouchListener() {
                                     @Override
                                     public boolean onTouch(View v, MotionEvent event) {
                                         if (MotionEvent.ACTION_DOWN != event.getAction())
                                             return false;

                                         nouPunt(g.obtenirGeneradorPunt(event), event, R.color.colorTouch);
                                         return true;
                                     }
                                 }
        );
    }


    private void nouPunt(GeneradorDePunt g, MotionEvent event, int color) {
        g.limits(vista.getWidth(), vista.getHeight());

        Punt p = g.nouPunt(event, getResources().getColor(color));
        cjtDePunts.afegeixPunt(p);  // Afegim al model
        vistaPunt.avisaVistaPunt(p);  // Avisem a la vista
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onMenuItemSelected(int featureId, MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_neteja:
                neteja();
                return true;
            case R.id.action_help:
                ajut();
                return true;
            case R.id.action_delDarrer:
                esborraDarrer();
                return true;
        }
        return super.onMenuItemSelected(featureId, item);
    }

    private void neteja() {
        cjtDePunts.neteja();
    }

    private void esborraDarrer() {
        cjtDePunts.esborraDarrer();
    }

    private void ajut() {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setMessage(R.string.textAjut)
                .setTitle(R.string.menu_ajut)
                .create()
                .show();
    }

    private void testIteracio() {
        CjtDePunts c = new CjtDePunts();

        c.afegeixPunt(new Punt(10, 10, Color.BLUE, 2));
        c.afegeixPunt(new Punt(1, 1, Color.BLUE, 2));
        c.afegeixPunt(new Punt(13, 31, Color.BLUE, 1));


        for (Punt p : c) {
            Log.i(TAG, p.toString());
        }
    }


}
