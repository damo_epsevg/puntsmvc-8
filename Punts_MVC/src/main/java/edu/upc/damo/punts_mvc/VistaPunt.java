package edu.upc.damo.punts_mvc;

import android.widget.TextView;

/**
 * Created by Josep M on 14/10/13.
 */
public class VistaPunt {
    TextView x, y;

    VistaPunt(TextView x, TextView y) {
        this.x = x;
        this.y = y;
    }

    void avisaVistaPunt(Punt p) {
        x.setText(String.valueOf(p.getX()));
        y.setText(String.valueOf(p.getY()));
    }

}
