package edu.upc.damo.punts_mvc;

import android.view.MotionEvent;

import java.util.Random;

/**
 * Created by Josep M on 21/10/2015.
 */


class GeneradorDePunt {

    static final int DIAMETRE = 25;
    static Random generador = new Random();

    int widthMax = 0;
    int heigthMax = 0;


    GeneradorDePunt obtenirGeneradorPunt(MotionEvent event) {
        if (event == null)
            return new GeneradorDePuntAleatori();
        else
            return new GeneradorDePuntCoordenades();
    }

    protected Punt obtePunt(MotionEvent event, int c) {
        return null;
    }

    public Punt nouPunt(MotionEvent event, int c) {
        Punt punt = obtePunt(event, c);
        return punt;
    }

    ;

    public void limits(int width, int height) {
        this.heigthMax = height;
        this.widthMax = width;
    }

    private class GeneradorDePuntAleatori extends GeneradorDePunt {


        @Override
        protected Punt obtePunt(MotionEvent event, int c) {
            /**
             * Genera un punt aleatori. El random retorna un valor entre 0 i 1, que considerem com una
             * fracció de l'espai disponible. Per això multipliquem el valor aleatori per la dimensió.
             * Per tal d'assegutar que el punt generat cap completament dins dels límits, hi sumem el diàmetre
             */

            return new Punt(generador.nextFloat() * widthMax + DIAMETRE,
                    generador.nextFloat() * heigthMax + DIAMETRE,
                    c, DIAMETRE);

        }
    }

    private class GeneradorDePuntCoordenades extends GeneradorDePunt {


        @Override
        protected Punt obtePunt(MotionEvent event, int c) {
            return new Punt(event.getX(), event.getY(), c, DIAMETRE);
        }
    }
}



